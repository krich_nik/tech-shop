# TechShop

A Java web application created for studing.

<h3> Online Store </h3>

The store has a Catalog of Goods for which it is necessary to realize the opportunity:
* Sorting by the name of the product (az, za)
* Sorting of goods at a price (from cheap to expensive, from expensive to cheap)
* sorting goods by novelty
* Sampling of goods by parameter (category, price gap, color, size, ...)
* The user views the catalog and can add products to the Trash
* After adding goods to the shopping cart, a registered user can make an Order
* For an unregistered user this option is not available
* After placing an order, it (the order) is assigned the status of 'registered'
* The user has a personal account in which he can view his orders

The system administrator has the following rights:
* adding / removing goods, changing information about the product
* blocking / unblocking the user
* transfer of the order from the status of 'registered' to 'paid' or 'canceled'

Expansion of the task:
* sending confirmation of registration by email
* captcha at registration
* generating reports

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

<ul>
 <li>Java 1.7</li>
 <li>Tomcat 8</li>
 <li>JSP/JSTL</li>
 <li>JDBC</li>
 <li>MySQl</li>
 <li>JavaMail API</li>
 <li>JExcel API</li>
 <li>Recaptcha4j</li>
</ul>

### Installing

A step by step series of examples that tell you have to get a development env running

1. Execute sql script to initialize database (TechShop/sql/db-create-mysql.sql)
2. Using Eclipse run the application server 
3. Go to localhost:8080/SummaryTask4/

## Running the tests

Execute TechShop/test/techshop/AllTest.java

## Built With

* [Ant](https://ant.apache.org/manual/) - Dependency Management

## Authors

* **Nikita Datsenko**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

