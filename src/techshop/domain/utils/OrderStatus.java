package techshop.domain.utils;

/**
 * Order statuses.
 * 
 * @author Nikita Datsenko
 *
 */
public enum OrderStatus {

	REGISTERED, PAID, CANCELLED;

	public String getName() {
		return name().toLowerCase();
	}

}
